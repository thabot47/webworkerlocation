console.log('Started worker');

self.postMessage('Hello');


let type = navigator.connection.effectiveType;

function updateConnectionStatus() {
  console.log(
    `Connection type changed from ${type} to ${navigator.connection.effectiveType}`
  );
  type = navigator.connection.effectiveType;
  
}

navigator.geolocation.getCurrentPosition(function(location) {
  console.log(location.coords.latitude);
  console.log(location.coords.longitude);
  console.log(location.coords.accuracy);
});

navigator.connection.addEventListener("change", updateConnectionStatus);


self.onmessage = (e) => {
  self.postMessage(`relayed from worker: ${e.data}`);
};
